declare namespace apar {
interface FileUploadBody{
	[key:string]:any;
	f?:FormFile; // 文件
}
interface Oauth2GithubBody{
	[key:string]:any;
	from?:string; // 从哪里发起的请求
	code:string; // The code you received as a response to Step 1.
}
interface UserAddBody{
	[key:string]:any;
	name?:string; // 姓名
	account?:string; // 账号
	email?:string; // 邮箱
	tel?:string; // 电话号码
	passwd?:string; // 密码
	avatar?:string; // 头像url
	profile?:string; // 个人介绍
	role?:string; // 角色
}
interface UserCodeCheckBody{
	[key:string]:any;
	title:string; // 邮箱/手机
	code:string; // 验证码
}
interface UserCodeSendBody{
	[key:string]:any;
	title:string; // 手机号或邮箱
	code?:string; // 图片验证码
}
interface UserEditBody{
	[key:string]:any;
	id?:number; // 用户ID
	name?:string; // 姓名
	account?:string; // 账号
	email?:string; // 邮箱
	ecode?:string; // 邮箱验证码
	tel?:string; // 电话号码
	tcode?:string; // 手机验证码
	passwd?:string; // 密码
	passwd0?:string; // 旧密码
	avatar?:string; // 头像url
	profile?:string; // 个人介绍
	lvl?:number; // 角色
	money?:number; // 金币
	sex?:number; // 性别
	birth_at?:number; // 生日
}
interface UserGetBody{
	[key:string]:any;
	id?:string; // 用户ID
}
interface UserListBody{
	[key:string]:any;
}
interface UserLoginBody{
	[key:string]:any;
	title:string; // 账号/邮箱/电话号码
	passwd:string; // 密码
}
interface UserLogoutBody{
	[key:string]:any;
}
interface UserQrLoginBody{
	[key:string]:any;
	token?:string; // 口令
}
interface UserRegisterBody{
	[key:string]:any;
	name?:string; // 用户名
	account?:string; // 账号
	title?:string; // 手机号或邮箱
	passwd:string; // 密码
	code?:string; // 验证码
	invite?:string; // 邀请码
}
interface UserSearchBody{
	[key:string]:any;
	user_id?:number; // 用户ID
	invite_id?:number; // 邀请用户ID
	email?:string; // 邮箱
	account?:string; // 账号
	page?:number; // 页码 默认:0
	pageSize?:number; // 分页大小 默认:10
	sortBy?:string; // 排序方式
	desc?:number; // 降序
}
interface UserWhoamiBody{
	[key:string]:any;
	force?:number; // 是否强制刷新个人信息
}
}