declare namespace db {
interface User {
	id?: number, 
	account?: string,  // 账号
	email?: string,  // 邮箱
	tel?: string,  // 手机号
	passwd?: string,  // 密码
	name?: string,  // 用户名
	avatar?: string,  // 头像
	profile?: string,  // 简介
	sex?: number,  // 性别:0-未知 1-男 2:女
	birth_at?: number,  // 生日
	lvl?: number,  // 权限
	money?: number,  // 总币数
	mCost?: number,  // 已使用币数
	rmb?: number,  // 总返利
	rmbCost?: number,  // 已提现钱数
	invite_id?: number,  // 邀请人
	invite_rmb?: number,  // 是否已送邀请人现金
	create_at: number,  // 注册时间
	login_at?: number,  // 最近登录时间
	unionid?: string,  // 微信唯一ID
	province?: string,  // 省份
	city?: string,  // 城市
	country?: string,  // 国家
	github_id?: number,  // 关联github的ID
}interface Verify {
	id?: number, 
	title?: string,  // 验证码标识
	code?: string,  // 验证码
	rest?: number,  // 剩余次数
	update_at?: number,  // 更新时间
}interface File {
	id?: number, 
	create_id?: number,  // 上传者
	ip?: string,  // 上传图片的IP
	ua?: string,  // 客户端信息
	name?: string,  // 文件名
	ext?: string,  // 文件后缀名
	create_at: number,  // 上传时间
}interface Notice {
	id?: number, 
	title: string,  // 通知标题
	content: string,  // 通知内容
	create_at: number,  // 创建时间
}interface Material {
	id?: number, 
	item_id: number,  // 商品ID
	level_one_category_id?: number,  // 一级类目ID 如: 21
	level_one_category_name?: string,  // 一级类目名称 如: 居家日用
	category_id?: number,  // 叶子类目id 如: 50025847
	category_name?: string,  // 叶子类目名称 如: 居家鞋
	commission_rate?: number,  // 佣金比率(%) 如: 9.0
	coupon_share_url?: string,  // 推广链接
	coupon_word?: string,  // 推广淘口令
	coupon_remain_count?: number,  // 优惠券剩余量 如: 74000
	coupon_total_count?: number,  // 券总量 如: 100000
	coupon_start_fee?: number,  // 如: 19.0
	coupon_amount: number,  // 券面额(元) 如: 5
	activity_id?: string,  // 券ID: 24e329b18d7f42f488bbd080fc0b1ae2| 京东: planId
	coupon_start_time?: number,  // 如: 1550678400000
	coupon_end_time?: number,  // 如: 1550937599000
	item_description?: string,  // 宝贝描述（推荐理由,不一定有） 如: 漏水速干 透气清凉| 京东: requestId
	nick?: string,  // 店铺信息-卖家昵称 如: 素实旗舰店
	pict_url?: string,  // 如: //img.alicdn.com/bao/uploaded/i1/3821849208/O1CN01c3WLYM2HtJSoLakDj_!!0-item_pic.jpg
	seller_id?: number,  // 卖家id 如: 3821849208
	shop_title?: string,  // 如: 素实旗舰店
	small_images?: string,  // 如: {"string":["//img.alicdn.com/i4/3821849208/O1CN012HtJRb0WauWbDJc_!!3821849208.jpg"]}
	title: string,  // 如: 买一送一防滑漏水浴室拖鞋女夏家用室内洗澡居家鞋情侣冲凉拖鞋男
	user_type?: number,  // 0表示集市，1表示商城 如: 1
	volume: number,  // 30天销量 如: 9856
	zk_final_price: number,  // 折扣价 如: 19.8
	hot?: number,  // 热度
}interface Mp {
	id?: number, 
	title?: string,  // 项目名称
	token: string, 
	appid: string, 
	secret: string, 
	encodingAESKey: string, 
	checkSignature: number, 
	rebate?: string,  // 返利比例,如: 0.5元
	rebateCoupon?: number,  // alimama:有券也返利
	nick?: string,  // 昵称
	tuling?: string,  // 图灵: apiKey
	create_id?: number,  // 创建者
}interface Replys {
	id?: number, 
	mid: number,  // 微信公众平台ID
	name?: string,  // 规则名称
	kind?: number,  // 0-关键词 1-全词 2-正则 3-表达式 4-关注 5-发卡
	r: string,  // 规则
	idx: number,  // 排序
	expired_at?: number,  // 过期时间
	value: string,  // 回复内容
	create_id?: number,  // 创建者
}interface History {
	id?: number, 
	uid: number,  // 用户ID
	item_id: number,  // 商品ID
	title: string,  // 如: 买一送一防滑漏水浴室拖鞋女夏家用室内洗澡居家鞋情侣冲凉拖鞋男
	pict_url?: string,  // 如: //img.alicdn.com/bao/uploaded/i1/3821849208/O1CN01c3WLYM2HtJSoLakDj_!!0-item_pic.jpg
	volume: number,  // 30天销量 如: 9856
	zk_final_price: number,  // 折扣价 如: 19.8
	coupon_amount?: number,  // 券面额(元) 如: 5 (0代表返利类)
	coupon_share_url?: string,  // 推广链接
	coupon_word?: string,  // 推广淘口令,非null代表用户点击领取过| 京东: 领券链接参见public/jd.html
	create_at: number,  // 历史创建时间
	commission_rate?: number,  // 返利比例,万分之一
	user_type?: number,  // 平台
}interface JdCodeLog {
	id?: number, 
	uid: number,  // 用户ID
	hid: number,  // 历史ID
	item_id: number,  // 商品ID
	create_at: number,  // 领取时间
}interface PddCodeLog {
	id?: number, 
	uid: number,  // 用户ID
	hid: number,  // 历史ID
	item_id: number,  // 商品ID
	create_at: number,  // 领取时间
}interface Payment {
	id?: number, 
	hid?: number,  // 历史ID
	uid?: number,  // 用户ID
	item_id: number,  // 商品ID
	auction_category: string,  // 类目名称
	title: string,  // 如: 买一送一防滑漏水浴室拖鞋女夏家用室内洗澡居家鞋情侣冲凉拖鞋男
	pict_url?: string,  // 如: //img.alicdn.com/bao/uploaded/i1/3821849208/O1CN01c3WLYM2HtJSoLakDj_!!0-item_pic.jpg
	item_num: number,  // 购买数量
	trade_parent_id: string,  // 淘宝父订单号
	trade_id: string,  // 淘宝订单号
	user_type?: number,  // 0表示集市，1表示商城 如: 1
	alipay_total_price: number,  // 付款金额(元), >0: 已付款 否则: 已失效
	pub_share_pre_fee: number,  // 效果预估, 不管是否取消都不变
	total_commission_fee?: number,  // 佣金金额, >0: 已结算
	coupon_amount?: number,  // 券面额, 绑定了历史则等于历史中的券面额,否则等于0
	create_at: number,  // 付款时间
	earning_at?: number,  // 签收时间
	finish_at?: number,  // 7天无理由时间,没过之前是0,过了无理由之后是给用户结算的时间
	pay_amount?: number,  // 返利金额
	match_at?: number,  // 匹配时间,>0代表订单匹配过
}interface Devices {
	id?: number, 
	device_id?: string,  // 设备ID
	ip?: string,  // 最后登录ID
	create_at: number,  // 创建时间
	update_at?: number,  // 更新时间
	cnt?: number,  // 使用次数
}interface Openwx {
	openid: string, 
	nickname?: string,  // 普通用户昵称
	sex?: number,  // 普通用户性别，1为男性，2为女性
	language?: string,  // 语言
	province?: string,  // 普通用户个人资料填写的省份
	city?: string,  // 普通用户个人资料填写的城市
	country?: string,  // 国家，如中国为CN
	headimgurl?: string,  // 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空
	privilege?: string,  // 用户特权信息，json数组，如微信沃卡用户为（chinaunicom）
	unionid: string,  // 用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的unionid是唯一的。
	token?: string,  // access_token
	refresh?: string,  // refresh_token
	expired_at?: number,  // token过期时间
	type?: number,  // 类型
	uid?: number,  // 用户ID
}interface PayAddress {
	id?: number, 
	uid?: number,  // 用户ID
	alipay?: string,  // 支付宝收款码url
	wechat?: string,  // 微信收款码url
	account?: string,  // 支付宝账号
	name?: string,  // 姓名
	create_at?: number,  // 创建时间
	update_at?: number,  // 修改时间
}interface PayRequest {
	id?: number, 
	uid?: number,  // 用户ID
	price: number,  // 提现金额
	x2?: number,  // 是否使用双倍券
	create_at?: number,  // 创建时间
	pay_at?: number,  // 提现时间
	mode?: number,  // 提现方式: 0-待处理 1-支付宝收款 2-微信收款 3-支付宝转账
	remark?: string,  // 提现备注信息
}interface AppVersion {
	id?: number, 
	version: string,  // 版本号
	description?: string,  // 版本信息
	url?: string,  // 版本URL
}interface Channel {
	id?: number, 
	create_id: number,  // 创建者
	name: string,  // 通道名称
	token?: string,  // 密钥
}interface ChannelUser {
	cid: number,  // 推送通道ID
	openid: string, 
	grp?: string,  // 分组信息
}interface Wxuser {
	openid: string, 
	nickname?: string, 
	sex?: number,  // 性别
	language?: string,  // 语言
	province?: string,  // 普通用户个人资料填写的省份
	city?: string,  // 普通用户个人资料填写的城市
	country?: string,  // 国家，如中国为CN
	headimgurl?: string,  // 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空
	unionid?: string,  // 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
	groupid?: number,  // 分组
	remark?: string,  // 备注
	subscribe_time?: number,  // 最后关注时间
	tagid_list?: string,  // 标签
	subscribe_scene?: number,  // 关注方式
	qr_scene?: number,  // 二维码扫码场景（开发者自定义）
	qr_scene_str?: string,  // 二维码扫码场景描述（开发者自定义）
	uid?: number,  // 用户ID
}interface Ctfile {
	id?: number, 
	email: string,  // 邮箱
	passwd: string,  // 密码
	token: string,  // 密钥
	create_id?: number,  // 创建者
	create_at: number,  // 创建时间
}interface Group {
	id?: number, 
	create_id?: number,  // 创建者
	name: string,  // 组名
	create_at: number,  // 创建时间
}interface Post {
	id?: number, 
	create_id?: number,  // 创建者
	edit_id?: number,  // 最近修改人
	head_id?: number,  // 负责人
	type?: number,  // 类型
	title: string,  // 标题
	create_at: number,  // 创建时间
	update_at: number,  // 修改时间
	plan_at?: number,  // 计划完成时间
	finish_at?: number,  // 完成时间
	cost?: number,  // 用时
	rate?: number,  // 进度
	path: string,  // 路径，默认/
	pid?: number,  // 父节点ID
	pow?: number,  // 帖子默认权限
	pow_id: number,  // 权限继承ID,该节点继承哪个祖先节点的权限
	child_cnt?: number,  // 子节点数量
	content?: string,  // 帖子内容
}interface AcPost {
	uid: number,  // 用户ID
	pid: number,  // 帖子ID
	pow: number,  // 权限位级:1查看,2回复,4下载,8添加,16修改,32移动,64修改评论,128删除,256删除评论,512管理
}interface Posttime {
	id?: number, 
	create_id: number,  // 工作者ID
	post_id: number,  // 帖子ID
	start_at: number,  // 开始时间
	end_at: number,  // 结束时间
	ip?: string,  // IP地址
}}