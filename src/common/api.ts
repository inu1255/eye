import axios from "axios";
import config from './config';

export async function pushNotice(id: string, token: string, text: string, url?: string): Promise<number> {
    let { data } = await axios.get(`https://quan2go.inu1255.cn/api/channel/push?id=${id}:${token}&text=${encodeURIComponent(text)}&url=${encodeURIComponent(url)}`);
    return data.data.n;
}
