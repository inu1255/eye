import * as utils from "./utils";
import * as config from "./config";
import WechatMp, { SnsUserInfoResponse, SUBSCRIBE_SCENES } from './wechat/mp';
import WechatOpen from "./wechat/open";

const store = utils.createLocal({
    access_token_expired_at: 0,
    access_token: "",
    jsapi_ticket_expired_at: 0,
    jsapi_ticket: "",
}, ".wx_mp_cache");

class WechatMpLocal extends WechatMp {
    saveAccessToken(access_token: string, expired_at: number) {
        store.access_token = access_token;
        store.access_token_expired_at = expired_at;
    }
    loadAccessToken() {
        if (store.access_token_expired_at > Date.now()) return store.access_token;
    }
    saveJSApiTicket(jsapi_ticket: string, expired_at: number) {
        store.jsapi_ticket = jsapi_ticket;
        store.jsapi_ticket_expired_at = expired_at;
    }
    loadJSApiTicket() {
        if (store.jsapi_ticket_expired_at > Date.now()) return store.jsapi_ticket;
    }
}

export const mp = new WechatMpLocal(config.wechat.appid, config.wechat.secret);
export const open = new WechatOpen(config.openwx.appid, config.openwx.secret);
export { SnsUserInfoResponse, SUBSCRIBE_SCENES }