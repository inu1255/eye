import { Router, RequestHandler } from "express";
import fs = require("fs");
import path = require("path");
import { devLogger } from "../common/log";
import config from '../lib/config';
import utils = require("../common/utils");
import './express_ext'

export interface GreenCheck {
    M: string;
    R: string;
    no: number;
}

export interface GreenData {
    [key: string]: any;
    no: number;
    msg?: string;
    data?: any;
}

export interface GreenFile {
    status: number; // 状态码
    contentType?: string; // content-type
    responseText?: string; // 返回文本
    response?: string; // 返回base64
}

export interface GreenCode {
    base: string;
    code: string;
}

function isGreenCode(v: any): v is GreenCode {
    return v && v.code && v.base
}

type GreenMethod = "all" | "get" | "head" | "post" | "put" | "delete" | "connect" | "options" | "trace" | "patch" | "move" | "copy" | "link" | "unlink" | "wrapped";
type GreenRet = { [key: string]: GreenFile | GreenData | GreenCode }

export interface GreenParam {
    lbl?: string; // 参数名
    rem?: string; // 参数注释
    need?: boolean | string | string[]; // 是否必传
    def?: any; // 默认值
    reg?: string; // 正则表达式
    type?: "int" | "float" | "number" | "json" | "array" | "str" | "file"; // 参数类型
    enum?: Array<any>; // 参数枚举值
    opts?: Array<any>; // 参数对应含义
    len?: [number, number] | number; // 长度限制 [6,32] 6 [0,100]
    range?: [number, number] | number; // 范围限制 [6,32] 6 [0,100]
}

export interface GreenApi {
    name: string; // 接口名称
    rem?: string; // 接口说明
    url: string; // 接口路径
    alias: string[]; // 接口路径别名
    method: GreenMethod[]; // 接口方法
    freq: number; // 每个IP多少毫秒之内只能调用一次
    additional: boolean; // 允许额外的参数
    params: { [key: string]: GreenParam }; // 接口参数
    pretreat: string; // 预处理
    grant: GreenCheck[]; // 权限
    check: GreenCheck[]; // 参数检查
    error: { [key: string]: string }; // 错误类型
    ret: GreenRet; // 返回值类型
}

export function obj2str(obj: any, deep: number = 2): string {
    if (!obj) return '';
    if (typeof obj === "string") return obj;
    for (let k in obj) {
        let v = obj[k]
        if (typeof v === "string") return v;
    }
    if (deep < 1) return;
    for (let k in obj) {
        let v = obj2str(obj[k], deep - 1)
        if (v) return v;
    }
}

function walkFiles(dir: string, fn: (filename: string) => void) {
    const files = fs.readdirSync(dir);
    for (let filename of files) {
        filename = path.join(dir, filename);
        let stat = fs.statSync(filename);
        if (stat.isDirectory()) {
            walkFiles(filename, fn);
        } else {
            fn(filename);
        }
    }
}

/**
 * 从body中清除字段
 * @param keys 字段列表
 */
function paramClean(keys: string[]): RequestHandler {
    const km = {};
    for (let k of keys) {
        km[k] = true;
    }
    return function(req, res, next) {
        for (let k in req.body) {
            if (!km[k]) {
                delete req.body[k];
            }
        }
        next();
    };
}

/**
 * 生成参数验证函数
 * @param k 参数key
 * @param param
 */
function paramCheck(k: string, param: GreenParam): RequestHandler {
    let { need, def, enum: enu, type: typ, opts } = param;
    if (need || def || param.len || param.range || param.reg || enu || typ || opts) {
        let lbl = param.lbl || param.rem;
        const name = lbl || k;
        let len = param.len ? (Array.isArray(param.len) ? param.len : [param.len]) : null;
        let range = param.range ? (Array.isArray(param.range) ? param.range : [param.range]) : null;
        let reg: RegExp;
        if (param.reg) {
            try {
                reg = new RegExp(param.reg);
            } catch (error) {
                devLogger.error(`parse param.${k}.reg error: ${error}`);
            }
        }

        function checkFn(body: { [x: string]: any }) {
            let value = body[k];
            if (def != null && value == null) {
                body[k] = def;
                return;
            }
            if (value == null || value === "") {
                if (need instanceof Array) {
                    for (let i = 0; i < need.length; i++) {
                        if (body[need[i]] == null)
                            return `${name}是必填项`;
                    }
                }
                else if (typeof need === "string" ? body[need] == null : need)
                    return `${name}是必填项`;
            } else {
                if (typ && value != null) {
                    switch (typ) {
                        case "int":
                            if (value == 'true') value = 1;
                            else if (value == 'false') value = 0;
                            value = +value;
                            if (isNaN(value)) return `${name}必须是整数`;
                            body[k] = Math.floor(value);
                            break;
                        case "float":
                            value = +value;
                            if (isNaN(value)) return `${name}必须是数字`;
                            body[k] = value;
                            break;
                        case "array":
                            if (typeof value !== "object") {
                                try {
                                    value = body[k] = JSON.parse(value);
                                } catch (error) {
                                    return `${name}类型必须是array`;
                                }
                            }
                            if (!(value instanceof Array)) return `${name}必须是数组`;
                            break;
                        case "str":
                            if (value && typeof value !== "string") {
                                body[k] = JSON.stringify(value);
                            }
                            break;
                        case "json":
                            if (typeof value !== "object") {
                                try {
                                    value = body[k] = JSON.parse(value);
                                } catch (error) {
                                    return `${name}类型必须是json`;
                                }
                            }
                            break;
                        case "file":
                            if (value.constructor.name != "File") {
                                return `${name}类型必须是file`;
                            }
                            break;
                        case "number":
                            if (typeof value !== typ) return `${name}类型必须是${typ}`;
                    }
                }
                if (len && value != null) {
                    if (value.length < len[0]) {
                        return `${name}长度需大于${len[0]}`;
                    }
                    if (len[1] > 0 && value.length > len[1]) {
                        return `${name}长度需小于${len[1]}`;
                    }
                }
                if (range && value != null) {
                    if (value < range[0]) {
                        return `${name}需大于${range[0]}`;
                    }
                    if (typeof range[1] === "number" && value > range[1]) {
                        return `${name}需小于${range[1]}`;
                    }
                }
                if (reg && !reg.test(value)) {
                    return `${name}格式不正确`;
                }
                if (enu && enu.indexOf(body[k]) < 0) {
                    return `${name}的值不能为${value}`;
                }
                if (opts && (value < 0 || value > opts.length)) {
                    return `${name}的值不在[0,${opts.length}]中`;
                }
            }
        }
        return function(req, res, next) {
            let msg = checkFn(req.body);
            if (msg) return res.err(400, msg);
            next();
        };
    }
}

/**
 * 生成预处理函数
 * @param condition 条件表达式 $U: 登录用户 $S: 当前会话 $B: POST参数
 */
function pretreatMake(condition: string): RequestHandler {
    condition = condition.replace(/{([USB])}/g, "$$$1").replace(/{}/g, "$$B");
    return function(req, res, next) {
        try {
            new Function("$B", "$U", "$S", `${condition}`)(req.body, req.session.user, req.session);
        } catch (error) {
            devLogger.error(`(${condition})`, error);
            return res.err(500, "预处理失败");
        }
        next();
    };
}

/**
 * 生成条件检查函数
 * @param condition 条件表达式 $U: 登录用户 $S: 当前会话 $B: POST参数
 * @param msg 错误信息
 */
function conditionCheck(condition: string, msg: string, no: number): RequestHandler {
    condition = condition.replace(/{([USB])}/g, "$$$1").replace(/{}/g, "$$B");
    return function(req, res, next) {
        if (condition == "$U" && !req.session.user) {
            return res.err(401);
        }
        try {
            if (!new Function("$B", "$U", "$S", `return (${condition})`)(req.body, req.session.user, req.session)) {
                return res.err(no, msg);
            }
        } catch (error) {
            devLogger.error(`(${condition})`, error);
            return res.err(no, msg);
        }
        next();
    };
}

/**
 * 生成检查中间件
 * @param check 检查规则
 * @param no 返回码
 */
function conditionChecks(check: GreenCheck[], no: number): RequestHandler[] {
    let checks = [];
    if (!Array.isArray(check))
        console.log(check)
    for (let item of check) {
        checks.push(conditionCheck(item.R, item.M, item.no || no));
    }
    return checks;
}

/**
 * 生成 接口失败时返回数据的函数
 * @param error 错误码对应的错误信息
 */
function makeSendErr(error: { [x: string]: string }) {
    error = Object.assign({}, config.error, error);
    return function(no: number, msg: string) {
        this._sent = true;
        this.json({ no, msg: msg || error[no] || "未知错误" });
    };
}

/**
 * 生成 接口成功时返回数据的函数
 * @param api
 */
function makeSendOk(api: GreenApi, filename?: string) {
    if (api)
        return function(data) {
            this._sent = true;
            if (typeof data === "number") return this.err(data);
            if (!data || !data.no) data = { no: 200, data };
            this.no = data.no;
            this.json(data);
            if (!api.ret)
                console.log(JSON.stringify(data, null, 2));
            if (0 && config.dev && api.ret) {
                let errors = utils.expect(data, api.ret);
                if (errors.length) {
                    devLogger.error(`数据与定义不一致: ${filename}`);
                    for (let err of errors) {
                        devLogger.warn(err);
                    }
                    console.log(JSON.stringify(data, null, 2));
                }
            }
        };
    return function(data) {
        this._sent = true;
        if (typeof data === "number") return this.err(data);
        if (!data || !data.no) data = { no: 200, data };
        this.no = data.no;
        this.json(data);
    };
}

export function parseApi(text: string, path: string) {
    let data, isJSON: boolean;
    try {
        data = JSON.parse(text)
        isJSON = true
    } catch (e) {
        try {
            data = eval(`(${text})`)
            isJSON = false
        } catch (error) {
            throw "api定义错误";
        }
    }
    if (!data.name) {
        throw "api定义缺少name";
    }
    let url: string = data.url
    if (path) {
        url = path[0] == '/' ? path : '/' + path;
    }
    let method: GreenMethod[]
    if (typeof data.method === "string") {
        if (data.method == "*") method = ["all"];
        else method = data.method.toLowerCase().split("|")
    } else {
        method = data.method.map(x => x.toUpperCase())
    }
    if (!method.length)
        throw "api定义缺少method";
    let alias: string[] = typeof data.alias === "string" ? [data.alias] : data.alias
    let params: { [key: string]: GreenParam } = data.params
    if (params) {
        for (let k in params) {
            let v = params[k]
            if (!v.lbl && v.rem && v.rem.length < 10) {
                v.lbl = v.rem
                delete v.rem;
            }
        }
    }
    let check: GreenCheck[]
    if (typeof data.check === "string")
        check = [{ R: data.check, M: "参数不正确", no: 400 }]
    else
        check = data.check
    let grant: GreenCheck[]
    if (typeof data.grant === "string")
        grant = [{ R: data.grant, M: "没有权限", no: 403 }]
    else
        grant = data.grant
    let ret: GreenRet
    if (data.ret) {
        if (!data.ret.no && !data.ret.status) {
            for (let k in data.ret) {
                let v = data.ret[k]
                if (v.base && Array.isArray(v.code))
                    v.code = v.code.join(';');
            }
            ret = data.ret
        }
        else
            ret = { base: data.ret }
    }
    let api: GreenApi = {
        name: data.name,
        rem: data.rem,
        url,
        alias,
        method,
        freq: data.freq,
        additional: data.additional,
        params,
        pretreat: data.pretreat,
        grant,
        check,
        error: data.error,
        ret,
    }
    return { isJSON, api }
}

export function parseRet(ret: GreenRet) {
    var data: { [key: string]: GreenFile | GreenData } = {}
    let codes = []
    for (let k in ret) {
        let v = ret[k]
        if (isGreenCode(v)) {
            codes.push(k);
            data[k] = null
        }
        else
            data[k] = v;
    }
    for (let k of codes) {
        let v = ret[k] as GreenCode
        var $B = JSON.parse(JSON.stringify(data[v.base]))
        v.code = v.code.replace(/{}/g, '$B')
        new Function('$B', v.code)($B)
        data[k] = $B;
    }
    return data;
}

/**
 * 从文件中获取接口定义
 * @param filename 接口定义文件名
 */
function readApi(filename: string, apiDir: string): GreenApi {
    let text = fs.readFileSync(filename, "utf8");
    try {
        let data = parseApi(text, filename.slice(apiDir.length, -5).replace(/\\/g, '/'))
        return data.api;
    } catch (e) {
        devLogger.warn(e, filename);
    }
}

/**
 * 通过api.json文件获取对应的 接口实现函数
 * @param filename 模块路径
 * @param fnKey 函数键
 */
function getHander(filename: string, fnKey: string): RequestHandler {
    var handler: RequestHandler;
    try {
        let mod = require(filename);
        // console.log(Object.keys(mod), key);
        if (mod && typeof mod[fnKey] === "function") handler = mod[fnKey].bind(mod);
    } catch (error) {
        devLogger.error(error);
    }
    return handler;
}

export class ApiBuilder {
    private router: Router;
    private mountpath: string;
    constructor(mountpath?: string) {
        this.mountpath = mountpath;
        this.router = Router();
    }
    walk(apiDir: string, routeDir: string) {
        if (apiDir.startsWith(".")) apiDir = path.join(apiDir);
        if (routeDir.startsWith(".")) routeDir = path.join(routeDir);
        walkFiles(apiDir, filename => {
            if (filename.endsWith(".json")) {
                const data = readApi(filename, apiDir);
                if (!data) return;
                // /person/login
                let modulePath = filename.slice(apiDir.length, -5);
                // ["","person","login"]
                let ss = modulePath.split(path.sep);
                // login
                let key = ss[ss.length - 1].replace(/[-_]\w/g, a => a[1].toUpperCase());
                // "/person"
                modulePath = ss.slice(0, ss.length - 1).join("/");
                // api/person
                modulePath = path.join(routeDir, modulePath);
                if (!/^(\.|\/)/.test(routeDir)) modulePath = "./" + modulePath;
                this.routeApi(data, getHander(modulePath, key), filename);
            }
        });
        return this;
    }
	/**
	 * 通过json接口文件生成接口并路由
	 * @param api api定义
	 * @param handler 接口实现函数
	 * @param filename 接口定义文件
	 */
    routeApi(data: GreenApi, handler: RequestHandler, filename?: string) {
        let methods = data.method.map(x => x.toLowerCase())
        // 接口定义没问题
        // 构造参数检查函数
        let checks: RequestHandler[] = [];
        if (data.freq) {
            // 访问频率控制
            let ipMap = {};
            checks.push(function(req, res, next) {
                var now = Date.now();
                var prev = ipMap[req.ip];
                if (prev && prev + data.freq > now) return res.err(500, "操作太频繁");
                ipMap[req.ip] = now;
                next();
            });
        }
        if (data.params) {
            // 清除接口定义中不存在的参数
            if (!data.additional)
                checks.push(paramClean(Object.keys(data.params)));
            // 参数检查
            for (let k in data.params) {
                let v = data.params[k];
                let checkFn = paramCheck(k, v);
                if (checkFn) {
                    checks.push(checkFn);
                }
            }
        }
        if (data.grant) {
            // 权限检查
            checks.push(...conditionChecks(data.grant, 403));
        }
        if (data.pretreat) {
            // 预处理
            checks.push(pretreatMake(data.pretreat));
        }
        if (data.check) {
            // check检查
            checks.push(...conditionChecks(data.check, 400));
        }
        const sendErr = makeSendErr(data.error);
        const sendOk = makeSendOk(config.dev ? data : null, filename);
        // 构造接口实现函数
        let uri = data.url;
        if (!handler) {
            devLogger.warn("define", data.method, uri, "---> Mock数据");
            handler = function(req, res) {
                console.log(data.ret);
                res._sent = true;
                res.json(data.ret || { no: 200 });
            };
        }
        let args: Array<string | RequestHandler> = [this.mountpath ? this.mountpath + uri : uri];
        // 初始化
        args.push(function(req, res, next) {
            res.err = sendErr;
            res.ok = sendOk;
            req.body = Object.assign({}, req.params, req.query, req.body, req.fields, req.files);
            next();
        });
        // 参数、权限检查
        for (let fn of checks) {
            args.push(fn);
        }
        // 开始路由
        args.push(function(req, res, next) {
            let ret;
            try {
                if (handler.constructor.name === "GeneratorFunction") {
                    ret = require('co')(handler(req, res, next));
                } else {
                    ret = handler(req, res, next);
                }
            } catch (err) {
                devLogger.error(err);
                if (!res.finished && !res._sent) {
                    if (config.dev) res.err(500, obj2str(err));
                    else res.err(500, "系统错误");
                }
                return;
            }
            // 返回 promise 则 then
            if (ret && typeof ret.then === "function") {
                ret.then(
                    function(data) {
                        if (!res.finished && !res._sent) res.ok(data);
                    },
                    function(err) {
                        if (typeof err === "number")
                            return res.err(err);
                        devLogger.error(err);
                        if (!res.finished && !res._sent) {
                            if (config.dev) res.err(500, obj2str(err));
                            else res.err(500, "系统错误");
                        }
                    }
                );
            } else if (!res.finished && !res._sent) res.ok(ret);
        });
        for (let method of methods) {
            devLogger.debug("define", method, args[0]);
            this.router[method].apply(this.router, args);
            if (data.alias) {
                for (let uri_alias of utils.arr(data.alias)) {
                    if (!uri_alias.startsWith('/')) {
                        uri_alias = path.join(uri, uri_alias);
                        uri_alias = this.mountpath ? this.mountpath + uri_alias : uri_alias
                    }
                    args[0] = uri_alias;
                    devLogger.debug("define", method, args[0]);
                    this.router[method].apply(this.router, args);
                }
            }
        }
        return this;
    }
    build() {
        return this.router;
    }
}
