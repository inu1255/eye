import config from "../lib/config";
import { createPool } from './greendb';
import { dbLogger } from "./log";

export default createPool(config.mysql, dbLogger, config.v)