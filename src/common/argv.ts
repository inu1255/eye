var argv: { [key: string]: any; _: string[] } = { _: [] };
for (let i = 2; i < process.argv.length; i++) {
    let s = process.argv[i];
    let key = '';
    if (s.slice(0, 2) == '--')
        key = s.slice(2)
    else if (s.slice(0, 1) == '-')
        key = s.slice(1)
    if (key) {
        let next = process.argv[i + 1]
        if (next != null && next[0] != '-') {
            argv[key] = next;
            i++;
        }
        else
            argv[key] = true;
    } else {
        argv._.push(s)
    }
}

export default argv;