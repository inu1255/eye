import { Fields, Files } from "formidable";

declare module "express-serve-static-core" {
    interface Request {
        fields: Fields;
        files: Files;
        ua: string;
    }
    interface Response {
        err: (no: number, msg?: string) => void;
        ok: (data: any) => void;
        no: number; // 返回码
        _sent: boolean; // 是否已经返回
    }
}
