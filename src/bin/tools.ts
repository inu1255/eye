import * as readline from 'readline';
import db from '../common/db';
import argv from '../common/argv';
import * as fs from 'fs-extra'
import * as path from 'path';
import * as utils from '../common/utils';
import config from '../lib/config';
import * as wx from '../lib/wx';

declare module "express-serve-static-core" {
    interface Request {
        ua: string;
    }
}

var cmdStr = "npx ts-node tools.ts -q";
function arg(label: string, key?: string, def?: string): Promise<string> {
    return new Promise(function(resolve, reject) {
        let v = argv[key];
        if (v == null) v = def;
        if (argv.q && v != null) { // 安静模式，不询问参数
            v = (v || "") + "";
            cmdStr += ` --${key}=${JSON.stringify(v)}`;
            return resolve(v);
        }
        let rd = readline.createInterface(process.stdin, process.stdout);
        rd.question(`${label}:${v == null ? `` : `(默认: ${v})`}\n`, function(value) {
            v = (value || v || "") + "";
            cmdStr += ` --${key}=${JSON.stringify(v)}`;
            resolve(v);
            rd.close();
        });
    }).then((x: string) => x.trim());
}

function log() {
    if (!cmdStr) return;
    console.log("======================");
    console.log(cmdStr);
    console.log("======================");
    cmdStr = "";
}

let cmds = [];
cmds.push({
    name: "微信菜单",
    async handler() {
        let data = await wx.mp.menuCreate({
            button: [{
                type: 'view',
                name: 'APP下载',
                url: 'https://quan2go.inu1255.cn/app'
            }, {
                type: 'view',
                name: '找券',
                url: 'https://quan2go.inu1255.cn/goods'
            }]
        })
        console.log(data)
    }
})
cmds.push({
    name: "OPEN微信头像",
    async handler() {
        let rows: db.Openwx[] = await db.execSQL(`select * from openwx where headimgurl=''`)
        let cnt = 0;
        for (let row of rows) {
            if (row.expired_at < Date.now()) {
                let data = await wx.open.refreshToken(row.refresh).catch(x => null)
                if (!data) {
                    // console.log(row.nickname, utils.format('YYYY-MM-DD hh:mm:ss', row.expired_at))
                    continue;
                }
                let token = row.token = data.access_token;
                let expired_at = Date.now() + data.expires_in * 1e3;
                await db.update('openwx', { token, expired_at }).where('openid', row.openid)
            }
            let info = await wx.open.snsUserInfo(row.token, row.openid)
            let { nickname, sex, language, city, province, country, headimgurl } = info;
            if (headimgurl) {
                await db.update('openwx', { nickname, sex, language, city, province, country, headimgurl }).where('openid', info.openid)
                cnt++;
            }
        }
        console.log(`更新${cnt}/${rows.length}条`)
    }
})

async function main() {
    for (let i = 0; i < cmds.length; i++) {
        var row = cmds[i];
        console.log(`${i + 1}: ${row.name}`);
    }
    console.log('-----------------------');
    let i = await arg('请选择功能(输入对应数字)', 'e');
    var cmd = cmds[+i - 1];
    if (!cmd) throw '功能不存在';
    await cmd.handler();
    log()
}

main.apply(null, argv._).then(function() {
    console.log("end");
    process.exit();
}).catch(function(err) {
    console.log(err);
    console.log("err end");
    process.exit();
});
