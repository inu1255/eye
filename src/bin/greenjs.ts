#!/usr/bin/env node
import * as fs from 'fs-extra';
import { GreenApi, GreenCheck } from '../common/green';
import * as utils from '../common/utils';
import argv from '../common/argv';

/**
 * 遍历文件夹
 * @param {string} dir 
 * @param {(filename:string,stat:fs.Stats)=>Promise<boolean>|boolean} fn 
 */
export function walk(dir: string, fn: (filename: string, stat: fs.Stats) => Promise<boolean> | boolean): Promise<boolean> {
    return new Promise((resolve, reject) => {
        fs.readdir(dir, function(err, filenames) {
            if (err) reject(err);
            else {
                let task = Promise.resolve(false);
                for (let filename of filenames) {
                    filename = dir + "/" + filename;
                    task = task.then(x => new Promise((resolve, reject) => {
                        fs.stat(filename, function(err, stat) {
                            if (err) reject(err);
                            else if (stat.isDirectory()) {
                                resolve(Promise.resolve(fn(filename, stat)).then(x => x || walk(filename, fn)));
                            } else {
                                resolve(fn(filename, stat));
                            }
                        });
                    }));
                }
                resolve(task);
            }
        });
    });
};

async function main() {
    let dir = argv._[0] || 'api';
    let output = argv._[1] || 'typings/apar/index.d.ts'
    let s = 'declare namespace apar {\n';
    const TYPES = {
        "number": "number",
        "int": "number",
        "float": "number",
        "json": "any",
        "array": "any[]",
        "file": "FormFile",
    };
    await walk(dir, async function(filename, stat) {
        if (stat.isFile() && filename.endsWith('.json')) {
            let url = filename.slice(dir.length)
            if (url[0] == '/') url = url.slice(1);
            url = url.slice(0, -5)
            let define: GreenApi = eval('(' + await fs.readFile(filename, 'utf8') + ')');
            s += `interface ${utils.CamelCase(url.replace(/\//g, '_'))}Body{\n`
            if (define.additional) s += `\n\t[key:string]:any;`
            for (let key in define.params) {
                let param = define.params[key]
                let type: string;
                if (param.enum) type = typeof param.enum[0];
                else if (param.opts) type = "number";
                else type = TYPES[param.type] || "string";
                let def = param.def == null ? "" : " 默认:" + param.def;
                if (!param.need) {
                    key = `${key}?`;
                }
                s += `\t${key}:${type}; // ${param.rem || param.lbl}${def}\n`;
            }
            s += `}\n`;
        }
        return false;
    })
    s += '}'
    await fs.writeFile(output, s)
}

main();