#!/usr/bin/env node
import argv from '../common/argv';
import * as fs from 'fs';
import * as path from 'path';
import df from '../sql';
import db from '../common/db';
import { Field, Constraint } from '../common/greendb';

let cmd = argv._[0]; // 命令

let cmds = {
    init() {
        let df_file = require.resolve('../sql.ts')
        if (!argv.force && fs.existsSync(df_file)) {
            return console.log(`${df_file} exists`), process.exit();
        }
        df.init(db).then(function() {
            var w = fs.createWriteStream(df_file);
            w.write(`import { TableBuilder } from "./common/greendb";\n`)
            w.write(`let df = new TableBuilder();\n`)

            for (let k in df.tables) {
                let v = df.tables[k]
                w.write(`\ndf.table('${v._name}', [\n`);
                let fset = new Set();
                for (let field of v._fields) {
                    if (field instanceof Field) {
                        w.write('\tdf');
                        var m;
                        if (field._type == 'int unsigned')
                            w.write(`.unsigned('${field._name}')`);
                        else if (m = /varchar\((\d+)\)/.exec(field._type))
                            w.write(`.varchar('${field._name}', ${m[1]})`);
                        else if (["text", "json", "float", "int", "bigint",].indexOf(field._type) >= 0)
                            w.write(`.${field._type}('${field._name}')`);
                        else
                            w.write(`.field('${field._name}', '${field._type}')`);
                        if (!field._null) w.write(`.notNull()`);
                        if (field._inc) w.write(`.auto_increment()`);
                        if (field._charset) w.write(`.charset('${field._charset}')`);
                        if (field._default != null) {
                            var t: any = parseFloat(field._default)
                            if (isNaN(t)) t = field._default;
                            w.write(`.default(${JSON.stringify(t)})`);
                        }
                        if (field._comment) w.write(`.comment('${field._comment}')`);
                        w.write(',\n');
                    } else if (field._type.toLowerCase() == "foreign") {
                        fset.add(field._field);
                    }
                }
                for (let field of v._fields) {
                    if (field instanceof Constraint) {
                        if (field._type.toLowerCase() == "foreign")
                            w.write(`\tdf.foreign('${field._field.toLowerCase()}').references('${field._ref_table}', '${field._ref_field}'),\n`);
                        else if (field._type || !fset.has(field._field)) // foreign默认会加索引，所以忽略
                            w.write(`\tdf.${(field._type || 'index').toLowerCase()}('${field._field}'),\n`);
                    }
                }
                w.write(`])`)
                if (v._engine && !/innodb/i.test(v._engine)) w.write(`.engine('${v._engine.toLowerCase()}')`)
                // if (v._inc) w.write(`.auto_increment(${v._inc})`);
                if (v._charset) w.write(`.charset('${v._charset}')`)
                if (v._comment) w.write(`.comment('${v._comment}')`);
                w.write(`;\n`)
            }
            w.write(`\nexport default df;`)
            w.end(function() {
                db.end()
            })
        })
    },
    merge() {
        df.merge(db, argv.force).then(function() {
            db.end();
        }, function(e) {
            console.error(e);
            db.end();
        })
    },
    param() {
        let table = argv._[1];
        if (!table) return console.log('需要指定table');
        let params = df.tables[table].toParams();
        console.log(JSON.stringify(params, null, 4));
    },
    ts() {
        let w = fs.createWriteStream(argv._[1] || 'typings/db/index.d.ts');
        w.write('declare namespace db {\n');
        for (let k in df.tables) {
            let v = df.tables[k];
            w.write(v.toTypescript());
        }
        w.write('}');
        w.end();
    },
    dart() {
        let outdir = argv._[1] || '.dart_output';
        if (!fs.existsSync(outdir))
            fs.mkdirSync(outdir);
        for (let k in df.tables) {
            let v = df.tables[k];
            fs.writeFileSync(path.join(outdir, v.name + '.dart'), v.toDart());
        }
    },
    api() {
        let table = argv._[1];
        if (!table) return console.log('需要指定table');
        let apiDir = argv.api || table.replace(/_/g, '/')
        let [dir, name] = apiDir.split('/')
        let t = df.tables[table];
        if (!fs.existsSync(`api`))
            fs.mkdirSync(`api`);
        if (!fs.existsSync(`api/${dir}`))
            fs.mkdirSync(`api/${dir}`);
        if (!fs.existsSync(`src`))
            fs.mkdirSync(`src`);
        if (!fs.existsSync(`src/routes`))
            fs.mkdirSync(`src/routes`);
        let file = `src/routes/${dir}.ts`;
        let head: string
        try {
            head = fs.readFileSync(file, 'utf8');
        } catch (e) {
            head = [
                `import db from "../common/db";`,
                `import { appLogger } from "../common/log";`,
                `import * as utils from "../common/utils";`,
                `import * as cofs from "fs-extra";`,
            ].join('\n') + '\n';
        }
        let text = '';
        let fn_exist = false;
        for (let ret of [
            t.toApiAdd(dir, name),
            t.toApiDel(dir, name),
            t.toApiList(dir, name),
        ]) {
            let file = `api/${dir}/${ret.name}.json`;
            if (!argv.force && fs.existsSync(file)) {
                console.log(file, 'exists')
                fs.writeFileSync(file + '_', JSON.stringify(ret.api, null, 4))
            }
            else
                fs.writeFileSync(file, JSON.stringify(ret.api, null, 4))
            let idx = text.indexOf(ret.data[0]);
            if (idx >= 0) fn_exist = true
            text += ret.data.join('\n') + '\n';
        }
        if (fn_exist) {
            let ext = path.extname(file)
            fs.writeFileSync(file.slice(0, -ext.length) + '_' + ext, text)
        }
        else
            fs.writeFileSync(file, head + text)
    },
    usage() {
        console.log(`usage:
    根据已有数据库,生成数据库定义文件,如果文件已存在则停止,如果指定了--force则覆盖
        greendb init [--force]
    ***** 以下功能需要数据库定义文件 *****
    生成升级sql,如果--force则会直接执行sql
        greendb merge [--force]
    指定表格，生成接口定义的params
        greendb param <table>
    生成typescript定义
        greendb ts <outfile.d.ts>
    生成dart定义
        greendb dart <outdir>
    指定表格，生成常用接口的定义和实现
        greendb api <table> [--api apiPrefix]
		`)
    }
}

cmds[cmd] ? cmds[cmd]() : cmds.usage();